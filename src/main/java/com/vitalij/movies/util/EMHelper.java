package com.vitalij.movies.util;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class EMHelper {
    public static <T> List<T> getList(String table, String name, Class<T> class_) {
        EntityManager em = EMFactory.getManager();
        TypedQuery<T> query = em.createQuery(
                "FROM " + table + " t WHERE t.name = :name" , class_);
        return query.setParameter("name", name).getResultList();
    }
}
