package com.vitalij.movies;

import com.vitalij.movies.entity.Movie;
import com.vitalij.movies.entity.Person;
import com.vitalij.movies.entity.Rating;
import com.vitalij.movies.util.EMFactory;
import com.vitalij.movies.util.EMHelper;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.time.LocalDate;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        EntityManager em = EMFactory.getManager();

        EntityTransaction transaction = null;
        transaction = em.getTransaction();

        transaction.begin();

        Movie movie1 = new Movie("Matrix", LocalDate.of(2000, 5, 17));
        Person person1 = new Person("Petras");
        Rating rating1 = new Rating(2.5);
        Movie movie2 = new Movie("Titanic", LocalDate.of(1995, 8, 21));
        Person person2 = new Person("Algis");
        Rating rating2 = new Rating(8.4);

        rating1.setPerson(person1);
        rating1.setMovie(movie1);
        rating2.setPerson(person2);
        rating2.setMovie(movie2);

        em.persist(movie1);
        em.persist(person1);
        em.persist(rating1);
        em.persist(movie2);
        em.persist(person2);
        em.persist(rating2);

        transaction.commit();

        Person personFromDb = em.createQuery("from Person p where p.name='Algis'", Person.class).getSingleResult();
        Movie movieFromDb = em.createQuery("from Movie m where m.name='Titanic'", Movie.class).getSingleResult();

        transaction.begin();

        Rating rating3 = new Rating(5.5);
        rating3.setPerson(personFromDb);
        rating3.setMovie(movieFromDb);
        em.persist(rating3);

        transaction.commit();

        List<Movie> list = EMHelper.getList("Movie", "Titanic", Movie.class);
        list.forEach(movie -> System.out.println(movie.getId()));

        em.close();
    }
}
