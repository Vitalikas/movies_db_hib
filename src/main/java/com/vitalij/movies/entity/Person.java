package com.vitalij.movies.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Rating> ratings = new HashSet<>();

    public void addRating(Rating rating) {
        ratings.add(rating);
        rating.setPerson(this);
    }

    public void removeRating(Rating rating) {
        ratings.remove(rating);
        rating.setPerson(null);
    }

    public Person(String name) {
        this.name = name;
    }
}
